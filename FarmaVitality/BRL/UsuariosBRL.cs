﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;


namespace BRL
{
    public class UsuariosBRL : AbstractBRL
    {


    public UsuariosDAL Dal;
    public  Usuario user;
     

        public Usuario Usuario { get => user; set => user = value; }
        public UsuariosDAL dal { get => Dal; set => Dal = value; }


       
        public  UsuariosBRL()
        {
            Dal= new UsuariosDAL();
        }


        public UsuariosBRL(Usuario user)
        {
            this.user = user;
            Dal = new UsuariosDAL(user);
        }

        public override void Delete()
        {
            Dal.Delete();
        }

        public override void Insert()
        {
            Dal.Insert();
        }

        public override DataTable Select()
        {
            return Dal.Select();
        }
        public  DataTable NombreUsuario()
        {
            return Dal.NombreUsuario();
        }
        public DataTable SELECTCI (string ci)
        {
            return Dal.SELECTCI(ci);
        }
        public override void Update()
        {
            Dal.Update();
        }


        public Usuario Get(int IdUsuario)
        {
            return Dal.Get(IdUsuario);
        }
        public DataTable Login(string Usuario, string password)
        {
            Dal = new UsuariosDAL();
            return Dal.Login(Usuario, password);
        }
        public void Password(int id, string passw)
        {
            dal.Password(id, passw);
        }
    }
}


