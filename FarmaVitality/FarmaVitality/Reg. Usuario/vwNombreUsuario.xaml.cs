﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BRL;
using DAL;
using System.Data;

namespace FarmaVitality.Reg._Usuario
{
    /// <summary>
    /// Lógica de interacción para vwNombreUsuario.xaml
    /// </summary>
    public partial class vwNombreUsuario : Window
    {
        public vwNombreUsuario()
        {
            InitializeComponent();
        }
        Common.Usuario user;
        UsuariosBRL brl;
        void LlenarDataGridUsuario()
        {
            try
            {
                brl = new UsuariosBRL();
                dgvNombreUsuario.ItemsSource = null;
                dgvNombreUsuario.ItemsSource = brl.NombreUsuario().DefaultView;
                dgvNombreUsuario.Columns[0].Visibility = Visibility.Hidden;
                dgvNombreUsuario.Columns[1].Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + "error al filtrar LlenarDataGridUsuario ");
            }
        }
        void BuscarCI()
        {
            try
            {
                brl = new UsuariosBRL();
                DataTable dt = brl.SELECTCI(txtCI.Text);
                dgvNombreUsuario.CurrentItem = dt;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void BtnLimiar_Click(object sender, RoutedEventArgs e)
        {
           

        }

        private void BtnVer_Click(object sender, RoutedEventArgs e)
        {
            LlenarDataGridUsuario();
            //BuscarCI();
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    
    }
}
